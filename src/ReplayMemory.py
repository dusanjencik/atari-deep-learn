import random

import numpy as np

from arguments import logger


class ReplayMemory:
    def __init__(self, size, args):
        self.size = size
        self.screens = np.empty((self.size, args.screen_height, args.screen_width), dtype=np.uint8)
        self.actions = np.empty(self.size, dtype=np.uint8)
        self.rewards = np.empty(self.size, dtype=np.integer)
        self.terminals = np.empty(self.size, dtype=np.bool)
        self.history_length = args.history_length
        self.dims = (args.screen_height, args.screen_width)
        self.batch_size = args.batch_size
        self.count = 0
        self.current = 0
        self.prestates = np.empty((self.batch_size, self.history_length) + self.dims, dtype=np.uint8)
        self.poststates = np.empty((self.batch_size, self.history_length) + self.dims, dtype=np.uint8)

        logger.info("Replay memory size: %d" % self.size)

    def _getState(self, index):
        assert self.count > 0, "replay memory is empty, use at least --random_steps 1"
        index %= self.count  # normalize index
        if index >= self.history_length - 1:
            return self.screens[(index - (self.history_length - 1)):(index + 1), ...]
        else:
            return self.screens[[(index - i) % self.count for i in reversed(range(self.history_length))], ...]

    def add(self, action, reward, screen, terminal):
        assert screen.shape == self.dims
        self.actions[self.current] = action
        self.rewards[self.current] = reward
        self.screens[self.current, ...] = screen
        self.terminals[self.current] = terminal
        self.count = max(self.count, self.current + 1)
        self.current = (self.current + 1) % self.size

    def _findRandomIndex(self):
        while True:
            index = random.randint(self.history_length, self.count - 1)
            if (index >= self.current > index - self.history_length) or \
                    (self.terminals[(index - self.history_length):index].any()):
                continue
            return index

    def getMinibatch(self):
        assert self.count > self.history_length
        # sample random indexes
        indexes = []
        while len(indexes) < self.batch_size:
            index = self._findRandomIndex()
            self.prestates[len(indexes), ...] = self._getState(index - 1)
            self.poststates[len(indexes), ...] = self._getState(index)
            indexes.append(index)
        actions = self.actions[indexes]
        rewards = self.rewards[indexes]
        terminals = self.terminals[indexes]
        return self.prestates, actions, rewards, self.poststates, terminals
