import os
import sys

import cv2
from ale_python_interface import ALEInterface

from arguments import logger


class Emulator:
    def __init__(self, args):
        self.ale = ALEInterface()
        if args.display_screen:
            self.ale.setBool("display_screen", True)
            if sys.platform == 'darwin':  # Mac OS
                import pygame
                pygame.init()
                self.ale.setBool('sound', False)  # Sound doesn't work on OSX
            elif args.sound:
                self.ale.setBool('sound', True)

        self.ale.setInt('frame_skip', args.frame_skip)
        self.ale.setFloat('repeat_action_probability', 0)
        self.ale.setBool('color_averaging', True)
        if args.random_seed:
            self.ale.setInt('random_seed', args.random_seed)
        if args.record_screen_path:
            if not os.path.exists(args.record_screen_path):
                logger.info("Creating folder %s", args.record_screen_path)
                os.makedirs(args.record_screen_path)
            logger.info("Recording screen to %s", args.record_screen_path)
            self.ale.setString('record_screen_dir', args.record_screen_path)

        self.ale.loadROM(args.rom)
        self.actions = self.ale.getMinimalActionSet()
        logger.info("Using minimal action set with size %d", self.getNumActions())
        self.dims = (args.screen_width, args.screen_height)

    def getNumActions(self):
        return len(self.actions)

    def restart(self):
        self.ale.reset_game()

    def doAction(self, actionIndex):
        return self.ale.act(self.actions[actionIndex])  # return reward

    def getScreen(self):
        return cv2.resize(self.ale.getScreenGrayscale(), self.dims)

    def isTerminal(self):
        return self.ale.game_over()
