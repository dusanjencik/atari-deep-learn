import random

import numpy as np


class Agent:
    def __init__(self, emulator, replayMemory, qNet, args):
        self.emulator = emulator
        self.memory = replayMemory
        self.qNet = qNet
        self.num_actions = self.emulator.getNumActions()
        self.random_starts = args.random_starts
        self.history_length = args.history_length
        self.dims = (args.screen_height, args.screen_width)
        self.batch_size = args.batch_size
        self.buffer = np.zeros((self.batch_size, self.history_length) + self.dims, dtype=np.uint8)

        self.exploration_rate_start = args.exploration_rate_start
        self.exploration_rate_end = args.exploration_rate_end
        self.exploration_decay_steps = args.exploration_decay_steps
        self.exploration_rate_test = args.exploration_rate_test
        self.total_train_steps = args.trained_steps

        self.train_frequency = args.train_frequency
        self.train_repeat = args.train_repeat

        self.callback = None

    def _addToScreenBuffer(self, screen):
        assert screen.shape == self.dims
        self.buffer[0, :-1] = self.buffer[0, 1:]
        self.buffer[0, -1] = screen

    def _restartRandom(self):
        self.emulator.restart()
        # perform random number of dummy actions to produce more stochastic games
        for i in xrange(random.randint(self.history_length, self.random_starts) + 1):
            self.emulator.doAction(0)
            screen = self.emulator.getScreen()
            terminal = self.emulator.isTerminal()
            assert not terminal, "terminal state occurred during random initialization"
            # add dummy states to buffer
            self._addToScreenBuffer(screen)

    def _explorationRate(self):
        # calculate decaying exploration rate
        if self.total_train_steps < self.exploration_decay_steps:
            return self.exploration_rate_start - self.total_train_steps * (
                self.exploration_rate_start - self.exploration_rate_end) / self.exploration_decay_steps
        else:
            return self.exploration_rate_end

    def step(self, exploration_rate):
        # exploration rate determines the probability of random moves
        if random.random() < exploration_rate:
            action = random.randrange(self.num_actions)
        else:
            # otherwise choose action with highest Q-value
            # buffer, where first item is the current state
            qvalues = self.qNet.predict(self.buffer)
            assert len(qvalues[0]) == self.num_actions
            # choose highest Q-value of first state
            action = np.argmax(qvalues[0])

        # perform the action
        reward = self.emulator.doAction(action)
        screen = self.emulator.getScreen()
        terminal = self.emulator.isTerminal()

        # add screen to buffer
        self._addToScreenBuffer(screen)

        # restart the game if over
        if terminal:
            self._restartRandom()

        # call callback to record statistics
        if self.callback:
            self.callback.on_step(action, reward, terminal, screen, exploration_rate)

        return action, reward, screen, terminal

    def play_random(self, random_steps):
        # play given number of steps
        for i in xrange(random_steps):
            # use exploration rate 1 = completely random
            self.step(1)

    def play(self, num_games):
        # just make sure there is history_length screens to form a state
        self._restartRandom()
        for i in xrange(num_games):
            # play until terminal state
            terminal = False
            while not terminal:
                action, reward, screen, terminal = self.step(self.exploration_rate_test)

    def train(self, train_steps, epoch=0):
        # play given number of steps
        for i in xrange(train_steps):
            # perform game step
            action, reward, screen, terminal = self.step(self._explorationRate())
            self.memory.add(action, reward, screen, terminal)
            # train after every train_frequency steps
            if self.memory.count > self.memory.batch_size and i % self.train_frequency == 0:
                # train for train_repeat times
                for j in xrange(self.train_repeat):
                    # train the network
                    self.qNet.train(self.memory.getMinibatch(), epoch)
            # increase number of training steps for epsilon decay
            self.total_train_steps += 1

    def test(self, test_steps):
        # just make sure there is history_length screens to form a state
        self._restartRandom()
        # play given number of steps
        for i in xrange(test_steps):
            # perform game step
            self.step(self.exploration_rate_test)
