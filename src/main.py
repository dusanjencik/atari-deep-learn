import sys

from QNet import QNet
from ReplayMemory import ReplayMemory
from Statistics import Statistics, getDataFromLogs
from Agent import Agent
# important for parsing arguments and access to them
from arguments import *
from Emulator import Emulator

logger = logging.getLogger(__name__)


# load data from last log
epoch_addition = 0
last_total_time = 0
if args.csv_file and os.path.exists(args.csv_file):
    epoch_addition, args.exploration_rate_start, args.trained_steps, last_total_time = getDataFromLogs(args)

# init
emulator = Emulator(args)
replayMemory = ReplayMemory(args.replay_size, args)
qNet = QNet(emulator.getNumActions(), args)
agent = Agent(emulator, replayMemory, qNet, args)
stats = Statistics(agent, qNet, replayMemory, emulator, args, last_total_time)

if args.load_weights:
    logger.info("Loading weights from %s" % args.load_weights)
    qNet.load_weights(args.load_weights)

if args.play_games:
    logger.info("Playing for %d games", args.play_games)
    stats.reset()
    agent.play(args.play_games)
    stats.write(0 + epoch_addition, "play")
    sys.exit()

if args.random_steps and not args.load_weights:
    logger.info("Playing %d random steps", args.random_steps)
    stats.reset()
    agent.play_random(args.random_steps)
    stats.write(0, "random")

# loop over epochs
for epoch in xrange(args.epochs):
    if args.max_computing_time and stats.get_total_time() >= args.max_computing_time:
        logger.info("Timeout after %d sec", args.max_computing_time)
        break
    logger.info("Epoch #%d", (epoch + 1 + epoch_addition))
    if args.train_steps:
        logger.info(" Training for %d steps", args.train_steps)
        stats.reset()
        timeout = agent.train(args.train_steps, epoch + epoch_addition)
        stats.write(epoch + 1 + epoch_addition, "train")

        if args.save_weights_prefix:
            filename = args.save_weights_prefix + "_%d.prm" % (epoch + 1 + epoch_addition)
            logger.info(" Saving weights to %s", filename)
            qNet.save_weights(filename)
    if args.test_steps:
        logger.info(" Testing for %d steps", args.test_steps)
        stats.reset()
        agent.test(args.test_steps)
        stats.write(epoch + 1 + epoch_addition, "test")

stats.close()
logger.info("All done. Finish.")
