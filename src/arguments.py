import logging
import os

import argparse

logging.root.handlers = []
logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s')


def str2bool(v):
    return v.lower() in ("yes", "true", "t", "1")


def __init__():
    root_path = "../"

    parser = argparse.ArgumentParser()
    envArg = parser.add_argument_group('Environment')
    envArg.add_argument("rom", default=root_path + "roms/breakout.bin", help="The path to ROM file.")
    envArg.add_argument("--display_screen", type=str2bool, default=True,
                        help="Display game screen during training and testing.")
    envArg.add_argument("--sound", type=str2bool, default=False, help="Play (or record) sound.")  # Don't work on Mac
    envArg.add_argument("--frame_skip", type=int, default=4, help="How many times to repeat each chosen action.")
    envArg.add_argument("--screen_width", type=int, default=84, help="Screen width after resize.")
    envArg.add_argument("--screen_height", type=int, default=84, help="Screen height after resize.")
    envArg.add_argument("--record_screen_path",
                        help="Record game screens under this path. Subfolder for each game is created.")

    memArg = parser.add_argument_group('Replay memory')
    memArg.add_argument("--replay_size", type=int, default=1000000, help="Maximum size of replay memory.")
    memArg.add_argument("--history_length", type=int, default=4, help="How many screen frames form a state.")

    netArg = parser.add_argument_group('Deep Q-learning network')
    netArg.add_argument("--learning_rate", type=float, default=0.00025, help="Learning rate.")
    netArg.add_argument("--discount_rate", type=float, default=0.99, help="Discount rate for future rewards.")
    netArg.add_argument("--batch_size", type=int, default=32, help="Batch size for neural network.")
    netArg.add_argument('--optimizer', choices=['rmsprop', 'adam', 'adadelta'], default='rmsprop',
                        help='Network optimization algorithm.')
    netArg.add_argument("--decay_rate", type=float, default=0.95,
                        help="Decay rate for RMSProp and Adadelta algorithms.")
    netArg.add_argument("--clip_error", type=float, default=1,
                        help="Clip error term in update between this number and its negative.")
    netArg.add_argument("--target_steps", type=int, default=10000,
                        help="Copy main network into target network after this many steps.")
    netArg.add_argument("--trained_steps", type=int, default=0, help="This number will be added to trained steps.")
    netArg.add_argument("--min_reward", type=float, default=-1, help="Minimum reward.")
    netArg.add_argument("--max_reward", type=float, default=1, help="Maximum reward.")
    netArg.add_argument("--batch_norm", type=str2bool, default=False, help="Use batch normalization in all layers.")

    neonArg = parser.add_argument_group('Neon')
    neonArg.add_argument('--backend', choices=['cpu', 'gpu'], default='cpu',
                         help='Backend type for Neon')  # default CPU computing
    neonArg.add_argument('--device_id', type=int, default=0, help='GPU device id (only used with GPU backend)')
    neonArg.add_argument('--datatype', choices=['float16', 'float32', 'float64'], default='float32',
                         help='Default floating point precision for backend [float64 for cpu only]')
    neonArg.add_argument('--stochastic_round', const=True, type=int, nargs='?', default=False,
                         help='Use stochastic rounding [will round to BITS number of bits if specified]')

    antArg = parser.add_argument_group('Agent')
    antArg.add_argument("--exploration_rate_start", type=float, default=1,
                        help="Exploration rate at the beginning of decay.")
    antArg.add_argument("--exploration_rate_end", type=float, default=0.1, help="Exploration rate at the end of decay.")
    antArg.add_argument("--exploration_decay_steps", type=float, default=1000000,
                        help="How many steps to decay the exploration rate.")
    antArg.add_argument("--exploration_rate_test", type=float, default=0.05,
                        help="Exploration rate used during testing.")
    antArg.add_argument("--train_frequency", type=int, default=4, help="Perform training after this many game steps.")
    antArg.add_argument("--train_repeat", type=int, default=1,
                        help="Number of times to sample minibatch during training.")
    antArg.add_argument("--random_starts", type=int, default=30,
                        help="Perform max this number of dummy actions after game restart, to produce more random game dynamics.")

    mainArg = parser.add_argument_group('Main loop')
    mainArg.add_argument("--random_steps", type=int, default=50000,
                         help="Populate replay memory with random steps before starting learning.")
    mainArg.add_argument("--train_steps", type=int, default=250000, help="How many training steps per epoch.")
    mainArg.add_argument("--test_steps", type=int, default=125000, help="How many testing steps after each epoch.")
    mainArg.add_argument("--epochs", type=int, default=200, help="How many epochs to run.")
    mainArg.add_argument("--max_computing_time", type=int,
                         help="After timeout (in seconds) computing will be terminated.")
    mainArg.add_argument("--play_games", type=int, default=0,
                         help="How many games to play, suppresses training and testing.")
    mainArg.add_argument("--load_weights", help="Load network from file.")
    mainArg.add_argument("--save_weights_prefix",
                         help="Save network to given file. Epoch and extension will be appended.")
    mainArg.add_argument("--csv_file", help="Write training progress to this file.")

    comArg = parser.add_argument_group('Common')
    comArg.add_argument("--random_seed", type=int, help="Random seed for repeatable experiments.")
    comArg.add_argument("--log_level", choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"], default="INFO",
                        help="Log level.")
    return parser.parse_args()


args = __init__()
logger = logging.getLogger()
logger.setLevel(args.log_level)

# checks
if not os.path.isfile(args.rom):
    raise Exception("Rom file " + args.rom + " doesn't exist.")

logger.info("Successfully loaded")
