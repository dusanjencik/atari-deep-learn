import csv
import os
import sys
import time

import numpy as np

from arguments import logger


class Statistics:
    def __init__(self, agent, net, mem, env, args, last_total_time):
        self.agent = agent
        self.net = net
        self.mem = mem
        self.env = env

        self.agent.callback = self
        self.net.callback = self

        self.csv_name = args.csv_file
        if self.csv_name:
            logger.info("Results are written to %s" % args.csv_file)
            exists = os.path.exists(args.csv_file)
            if exists:
                self.csv_file = open(self.csv_name, "a")
            else:
                self.csv_file = open(self.csv_name, "wb")
            self.csv_writer = csv.writer(self.csv_file)
            if not exists:
                self.csv_writer.writerow((
                    "epoch",
                    "phase",
                    "steps",
                    "nr_games",
                    "average_reward",
                    "min_game_reward",
                    "max_game_reward",
                    "last_exploration_rate",
                    "total_train_steps",
                    "replay_memory_count",
                    "meanq",
                    "meancost",
                    "weight_updates",
                    "total_time",
                    "epoch_time",
                    "steps_per_second"
                ))
                self.csv_file.flush()
            else:
                logger.info("Continuing writing stats to %s", args.csv_file)

        self.start_time = time.clock() - last_total_time
        self.start_time_clear = time.clock()
        self.validation_states = None

    def reset(self):
        self.epoch_start_time = time.clock()
        self.num_steps = 0
        self.num_games = 0
        self.game_rewards = 0
        self.average_reward = 0
        self.min_game_reward = sys.maxint
        self.max_game_reward = -sys.maxint - 1
        self.last_exploration_rate = 1
        self.average_cost = 0

    # callback for agent
    def on_step(self, action, reward, terminal, screen, exploration_rate):
        self.game_rewards += reward
        self.num_steps += 1
        self.last_exploration_rate = exploration_rate

        if terminal:
            self.num_games += 1
            self.average_reward += float(self.game_rewards - self.average_reward) / self.num_games
            self.min_game_reward = min(self.min_game_reward, self.game_rewards)
            self.max_game_reward = max(self.max_game_reward, self.game_rewards)
            self.game_rewards = 0

    def on_train(self, cost):
        self.average_cost += (cost - self.average_cost) / self.net.train_iterations

    def get_total_time(self):
        return time.clock() - self.start_time_clear

    def write(self, epoch, phase):
        current_time = time.clock()
        total_time = current_time - self.start_time
        epoch_time = current_time - self.epoch_start_time
        steps_per_second = self.num_steps / epoch_time

        if self.num_games == 0:
            self.num_games = 1
            self.average_reward = self.game_rewards

        if self.validation_states is None and self.mem.count > self.mem.batch_size:
            # sample states for measuring Q-value dynamics
            prestates, actions, rewards, poststates, terminals = self.mem.getMinibatch()
            self.validation_states = prestates

        if self.csv_name:
            if self.validation_states is not None:
                qvalues = self.net.predict(self.validation_states)
                maxqs = np.max(qvalues, axis=1)
                assert maxqs.shape[0] == qvalues.shape[0]
                meanq = np.mean(maxqs)
            else:
                meanq = 0

            self.csv_writer.writerow((
                epoch,
                phase,
                self.num_steps,
                self.num_games,
                self.average_reward,
                self.min_game_reward,
                self.max_game_reward,
                self.last_exploration_rate,
                self.agent.total_train_steps,
                self.mem.count,
                meanq,
                self.average_cost,
                self.net.train_iterations,
                total_time,
                epoch_time,
                steps_per_second
            ))
            self.csv_file.flush()

        logger.info("  num_games: %d, average_reward: %f, min_game_reward: %d, max_game_reward: %d" %
                    (self.num_games, self.average_reward, self.min_game_reward, self.max_game_reward))
        logger.info("  last_exploration_rate: %f, epoch_time: %ds, steps_per_second: %d" %
                    (self.last_exploration_rate, epoch_time, steps_per_second))

    def close(self):
        if self.csv_name:
            self.csv_file.close()


def getDataFromLogs(args):
    assert os.path.exists(args.csv_file)
    with open(args.csv_file, "rb") as f:
        lines = f.readlines()
        lastTest = lines[-1].rstrip().split(',')
        lastTrain = lines[-2].rstrip().split(',')
        epoch = int(lastTrain[0])
        last_exploration_rate = float(lastTrain[7])
        total_train_steps = int(lastTrain[8])
        total_time = float(lastTest[13])
        return epoch, last_exploration_rate, total_train_steps, total_time
