#!/usr/bin/env bash

if [[  $# -le 1 || $1 == "--help" || $1 == "-h" ]]
then
    echo "Usage:"
    echo -e "\t./test.sh snapshots/{nameOfSnapshot}.prm roms/{nameOfRom}.bin [otherParams]"
    exit 1
fi

snapshot=$1

rom=$2
full=${rom##*/}
game=${full%.*}
shift
shift

python src/main.py --random_steps 0 --train_steps 0 --epochs 1 --load_weights $snapshot $rom $*
