#!/usr/bin/env bash

if [[  $# -le 0 || $1 == "--help" || $1 == "-h" ]]
then
    echo "Usage:"
    echo -e "\t./train.sh ./roms/{nameOfRom}.bin [otherParams]"
    exit 1
fi

mkdir -p results
mkdir -p snapshots

file=$1
full=${file##*/}
game=${full%.*}

if [[ -z $2 ]] || [[ $2 == -* ]]; then
  snapshots=snapshots/$game
  results=results/$game.csv
  shift
else
  # additional parameter is experiment label
  label=$2
  snapshots=snapshots/${game}_${label}
  results=results/${game}_${label}.csv
  shift
  shift
fi

python src/main.py --save_weights_prefix $snapshots --csv_file $results $file $*
