#!/usr/bin/env bash

# Commented parts are for first usage.


echo "adding dependecies"

ROOT_FOLDER="/storage/plzen1/home/dusanjencik/projects/AtariDeepLearn"
cd ${ROOT_FOLDER}

# add modules
echo "add modules"
module add python27-modules-intel
module add opencv-2.4
module add cuda-7.5

# sets pip path for --user option
echo "sets PATHs"
export PYTHONUSERBASE="/storage/plzen1/home/dusanjencik/.local"
export PATH=$PYTHONUSERBASE/bin:$PATH
export PYTHONPATH=$PYTHONUSERBASE/lib/python2.7/site-packages:$PYTHONPATH
export CUDA_ROOT="/storage/plzen1/home/dusanjencik/projects/pycuda/cuda"

# instalace arcadeLearnEnv.
echo "adding ALE to python"
cd ..
#git clone https://github.com/mgbellemare/Arcade-Learning-Environment.git
cd ./Arcade-Learning-Environment/
#module add cmake-2.8
#mkdir build
#cmake -DUSE_SDL=OFF -DUSE_RLGLUE=OFF -DBUILD_EXAMPLES=OFF .
#make
pip install --user .

# instalace neon
echo "adding neon to python"
cd ..
#git clone https://github.com/NervanaSystems/neon.git
cd neon
#make
pip install --user .

echo "adding cuda to python"
pip install --user pycuda
pip install --user scikit-cuda

cd ${ROOT_FOLDER}



echo "done"
