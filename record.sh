#!/usr/bin/env bash

if [[  $# -le 1 || $1 == "--help" || $1 == "-h" ]]
then
    echo "Usage:"
    echo -e "\t./record.sh snapshots/{nameOfSnapshot}.prm roms/{nameOfRom}.bin [{nameOfOuput}]"
    exit 1
fi

snapshot=$1

rom=$2
full=${rom##*/}
game=${full%.*}
file=${full%.*}
if [ ! -z $3 ]; then
    game=$3
    file=video_$game_$3
    shift
fi
shift
shift


rm -r videos/$game
python src/main.py --play_games 1 --record_screen_path videos/$game --load_weights $snapshot $rom $*
avconv -r 60 -i videos/$game/%06d.png -f mov -c:v libx264 -y videos/$file.mov
