#!/usr/bin/env bash

if [[  $# -eq 0 || $1 == "--help" || $1 == "-h" ]]
then
    echo "Usage (you can choose one of them):"
    echo "for plot of average_reward, meanq, number_games, meancost:"
    echo -e "\t./plot.sh results/{nameOfLogFile}.csv"
    echo -e "\tis equivalent for:"
    echo -e "\t./plot.sh results/{nameOfLogFile}.csv results/{nameOfOutput} --fields average_reward,meanq,nr_games,meancost"
    echo "for plot specific field(s):"
    echo -e "\t./plot.sh results/{nameOfLogFile}.csv results/{nameOfOutput} --fields {nameOfField(s)}"
    echo "for plot all fields"
    echo -e "\t./plot.sh results/{nameOfLogFile}.csv results/all --fields all"
    exit 1
fi

file=${1%.*}

if [ ! -z $2 ]; then
    name=$2
    file=$1
    shift
    shift
    python src/plot.py ${file} --eps_file ${name}.eps $*
else
    python src/plot.py --eps_file ${file}.eps $*
fi


